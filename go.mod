module gitlab.com/X3ntr/pensivenotes-sanderf

go 1.14

// +heroku goVersion go1.14

require (
		github.com/mattn/go-sqlite3 v1.14.4
		github.com/gorilla/mux v1.8.0
)